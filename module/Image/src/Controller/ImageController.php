<?php

namespace Image\Controller;

use Common\AbstractClasses\AppAbstractRestfulController;
use Laminas\View\Model\JsonModel;

class ImageController extends AppAbstractRestfulController
{

    public function __construct(

    ) {

    }

    public function create($data)
    {
        $request = $this->getRequest();
        $files = $request->getFiles();
        $folder = (!empty($data['folder'])) ? $data['folder']: 'product-images';

        $indexPath = $_SERVER['DOCUMENT_ROOT'];
        $imagesServerName = 'http://local-images.j-intuitive-simple-pos-and-inventory-system.com/';
        $imagesServerRootFolder = '/j-intuitive-simple-pos-and-inventory-system-images/';
        $uploadDirectory =  dirname(dirname($indexPath)) . $imagesServerRootFolder . '/' . $folder . '/';
        $currentTimeStamp = time();
        $savedFiles = [];

        foreach ($files as $file) {
            $fileName = pathinfo($file['name'], PATHINFO_FILENAME);
            $fileExtension = pathinfo($file['name'], PATHINFO_EXTENSION);
            $newFileName =  $fileName . '_' . $currentTimeStamp . '.' .  $fileExtension;
            $uploadPath = $uploadDirectory . $newFileName;
            move_uploaded_file($file['tmp_name'], $uploadPath);

            $imageURLInServer = $imagesServerName . '/' . $folder . '/' . $newFileName;
            array_push($savedFiles, ['url' => $imageURLInServer]);
        }

        return new JsonModel([
            'success' => true,
            'data' => [
                'images' => $savedFiles
            ]
        ]);
    }
}