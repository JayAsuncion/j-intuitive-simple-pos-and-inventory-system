<?php

namespace Image\ServiceFactory\Controller;

use Image\Controller\ImageController;
use Psr\Container\ContainerInterface;

class ImageControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new ImageController(
        );
    }
}