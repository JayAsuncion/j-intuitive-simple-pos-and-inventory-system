<?php

/**
 * @see       https://github.com/laminas-api-tools/api-tools-skeleton for the canonical source repository
 * @copyright https://github.com/laminas-api-tools/api-tools-skeleton/blob/master/COPYRIGHT.md
 * @license   https://github.com/laminas-api-tools/api-tools-skeleton/blob/master/LICENSE.md New BSD License
 */

namespace Product;

use Laminas\Router\Http\Segment;
use Product\Controller\ProductController;
use Product\Controller\ProductStockController;
use Product\Model\ProductsTable;
use Product\Model\ProductStocksTable;
use Product\ServiceFactory\Controller\ProductControllerFactory;
use Product\ServiceFactory\Controller\ProductStockControllerFactory;
use Product\ServiceFactory\Model\ProductsTableFactory;
use Product\ServiceFactory\Model\ProductStocksTableFactory;

return [
    'router' => [
        'routes' => [
            'products' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/products[/:id]',
                    'defaults' => [
                        'controller' => ProductController::class
                    ],
                ],
            ],
            'product-stocks' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/product-stocks[/:id]',
                    'defaults' => [
                        'controller' => ProductStockController::class
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            ProductController::class => ProductControllerFactory::class,
            ProductStockController::class => ProductStockControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            ProductsTable::class => ProductsTableFactory::class,
            ProductStocksTable::class => ProductStocksTableFactory::class,
        ],
        'invokables' => [

        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
