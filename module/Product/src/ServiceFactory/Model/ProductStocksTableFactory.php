<?php

namespace Product\ServiceFactory\Model;

use Laminas\Db\ResultSet\ResultSet;
use Laminas\Db\TableGateway\TableGateway;
use Product\Model\ProductStocks;
use Product\Model\ProductStocksTable;
use Psr\Container\ContainerInterface;

class ProductStocksTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('j-intuitive-simple-pos-and-inventory-system');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new ProductStocks());

        $tableGateway = new TableGateway(
            'product_stocks',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new ProductStocksTable($tableGateway);
    }
}