<?php

namespace Product\ServiceFactory\Model;

use Laminas\Db\ResultSet\ResultSet;
use Laminas\Db\TableGateway\TableGateway;
use Product\Model\Product;
use Product\Model\ProductsTable;
use Psr\Container\ContainerInterface;

class ProductsTableFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $dbAdapter = $container->get('j-intuitive-simple-pos-and-inventory-system');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Product());

        $tableGateway = new TableGateway(
            'products',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new ProductsTable($tableGateway);
    }
}