<?php

namespace Product\ServiceFactory\Controller;

use Product\Controller\ProductController;
use Product\Model\ProductsTable;
use Product\Model\ProductStocksTable;
use Psr\Container\ContainerInterface;

class ProductControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $ProductsTable = $container->get(ProductsTable::class);
        $ProductStocksTable = $container->get(ProductStocksTable::class);

        return new ProductController(
            $ProductsTable,
            $ProductStocksTable
        );
    }
}