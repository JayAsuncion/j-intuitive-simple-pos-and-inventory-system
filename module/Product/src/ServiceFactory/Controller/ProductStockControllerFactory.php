<?php

namespace Product\ServiceFactory\Controller;

use Product\Controller\ProductStockController;
use Product\Model\ProductsTable;
use Product\Model\ProductStocksTable;
use Psr\Container\ContainerInterface;

class ProductStockControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $ProductsTable = $container->get(ProductsTable::class);
        $ProductStocksTable = $container->get(ProductStocksTable::class);

        return new ProductStockController(
            $ProductsTable,
            $ProductStocksTable
        );
    }
}