<?php

namespace Product\Model;

use Common\AbstractClasses\AppAbstractTable;
use Laminas\Db\TableGateway\TableGateway;

class ProductStocksTable extends AppAbstractTable
{
    public function __construct(TableGateway  $TableGateway)
    {
        parent::__construct($TableGateway);

        $this->primary_key = 'product_stock_id';
    }

    public function getAllProductStocksByProductID($productID)
    {
        $select = $this->TableGateway->getSql()->select();
        $select->columns(['*']);
        $select->where(['product_id' => $productID]);
        $resultSet = $this->TableGateway->selectWith($select);

        return iterator_to_array($resultSet->getDataSource());
    }
}