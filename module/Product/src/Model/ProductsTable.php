<?php

namespace Product\Model;

use Common\AbstractClasses\AppAbstractTable;
use Laminas\Db\Sql\Expression;
use Laminas\Db\TableGateway\TableGateway;

class ProductsTable extends AppAbstractTable
{
    public function __construct(TableGateway $TableGateway)
    {
        parent::__construct($TableGateway);

        $this->primary_key = 'product_id';
    }

    public function getAllProductsAndProductStocks()
    {
        $columns = [
            'product_id', 'barcode', 'product_name', 'unit', 'image_url', 'delete_flag',
            'low_stock_count', 'high_stock_count',
            'quantity' => new Expression('ps.quantity'),
            'unit_cost' => new Expression('ps.unit_cost'),
            'unit_price' => new Expression('ps.unit_price')
        ];
        $where = [
            'delete_flag' => 'n'
        ];
        $select = $this->TableGateway->getSql()->select();
        $select->columns($columns);
        $select->where($where);
        $select->join(['ps' => 'product_stocks'], 'products.product_id = ps.product_id', [], 'INNER');
        $resultSet = $this->TableGateway->selectWith($select);

        return iterator_to_array($resultSet->getDataSource());
    }

    public function getAllProductsAndProductStocksByColumn($columnName, $columnValue)
    {
        $columns = [
            'product_id', 'barcode', 'product_name', 'unit', 'image_url', 'delete_flag',
            'low_stock_count', 'high_stock_count',
            'product_stock_id' => new Expression('ps.product_stock_id'),
            'quantity' => new Expression('ps.quantity'),
            'unit_cost' => new Expression('ps.unit_cost'),
            'unit_price' => new Expression('ps.unit_price')
        ];
        $select = $this->TableGateway->getSql()->select();
        $select->columns($columns);
        $select->where
            ->equalTo('delete_flag', 'n')
            ->and
            ->like($columnName, '%' . $columnValue . '%');
        $select->join(['ps' => 'product_stocks'], 'products.product_id = ps.product_id', [], 'INNER');
        $resultSet = $this->TableGateway->selectWith($select);

        return iterator_to_array($resultSet->getDataSource());
    }

    public function getProductsAndProductStocksByProductID($productID)
    {
        $columns = [
            'product_id', 'barcode', 'product_name', 'unit', 'image_url', 'delete_flag',
            'low_stock_count', 'high_stock_count',
            'product_stock_id' => new Expression('ps.product_stock_id'),
            'quantity' => new Expression('ps.quantity'),
            'unit_cost' => new Expression('ps.unit_cost'),
            'unit_price' => new Expression('ps.unit_price')
        ];
        $where = [
            'products.product_id' => $productID,
            'delete_flag' => 'n'
        ];
        $select = $this->TableGateway->getSql()->select();
        $select->columns($columns);
        $select->where($where);
        $select->join(['ps' => 'product_stocks'], 'products.product_id = ps.product_id', [], 'INNER');
        $resultSet = $this->TableGateway->selectWith($select);

        return $resultSet->getDataSource()->current();
    }
}