<?php

namespace Product\Model;

use Common\AbstractClasses\AppAbstractModel;

class ProductStocks extends AppAbstractModel
{
    public $product_stock_id;
    public $product_id;
    public $quantity;
    public $unit_cost;
    public $unit_price;

    private $defaultInsertColumns = ['product_id', 'quantity', 'unit_cost', 'unit_price'];
    private $defaultUpdateColumns = ['unit_cost', 'unit_price'];

    public function exchangeArray($data)
    {
        $this->product_stock_id = !empty($data['product_stock_id']) ? $data['product_stock_id'] : 0;
        $this->product_id = !empty($data['product_id']) ? $data['product_id'] : 0;
        $this->quantity = !empty($data['quantity']) ? $data['quantity'] : 0;
        $this->unit_cost = !empty($data['unit_cost']) ? $data['unit_cost'] : 0;
        $this->unit_price = !empty($data['unit_price']) ? $data['unit_price'] : 0;
    }

    public function filterValuesForUpdate($data)
    {
        $filteredData = [];

        if (!empty($data['quantity'])) {
            $filteredData['quantity'] = $data['quantity'];
        }

        if (!empty($data['unit_cost'])) {
            $filteredData['unit_cost'] = $data['unit_cost'];
        }

        if (!empty($data['unit_price'])) {
            $filteredData['unit_price'] = $data['unit_price'];
        }

        return $filteredData;
    }

    public function getDefaultInsertColumns()
    {
        return $this->defaultInsertColumns;
    }

    public function getDefaultUpdateColumns()
    {
        return $this->defaultUpdateColumns;
    }
}