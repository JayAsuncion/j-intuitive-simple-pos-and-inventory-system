<?php

namespace Product\Model;

use Common\AbstractClasses\AppAbstractModel;

class Product extends AppAbstractModel
{
    public $product_id;
    public $barcode;
    public $product_name;
    public $unit;
    public $image_url;
    public $low_stock_count;
    public $high_stock_count;
    public $product_brand_id;
    public $product_category_id;
    public $delete_flag;

    private $defaultInsertColumns = ['barcode', 'product_name', 'unit', 'image_url', 'low_stock_count', 'high_stock_count', 'product_brand_id', 'product_category_id', 'delete_flag'];
    private $defaultUpdateColumns = ['barcode', 'product_name', 'unit', 'image_url', 'low_stock_count', 'high_stock_count', 'product_brand_id', 'product_category_id', 'delete_flag'];

    public function exchangeArray($data)
    {
        $this->product_id = !empty($data['product_id']) ? $data['product_id'] : 0;
        $this->barcode = !empty($data['barcode']) ? $data['barcode'] : '';
        $this->product_name = !empty($data['product_name']) ? $data['product_name'] : '';
        $this->unit = !empty($data['unit']) ? $data['unit'] : '';
        $this->image_url = !empty($data['image_url']) ? $data['image_url'] : '';
        $this->low_stock_count = !empty($data['low_stock_count']) ? $data['low_stock_count'] : 0;
        $this->high_stock_count = !empty($data['high_stock_count']) ? $data['high_stock_count'] : 0;
        $this->product_brand_id = !empty($data['product_brand_id']) ? $data['product_brand_id'] : 0;
        $this->product_category_id = !empty($data['product_category_id']) ? $data['product_category_id'] : 0;
        $this->delete_flag = (!empty($data['delete_flag']) && $data['delete_flag'] === 'y') ? 'y' : 'n';
    }

    public function filterValues($data)
    {
        $values = [];

        $values['barcode'] = !empty($data['product_name']) ? $data['barcode'] : '';
        $values['product_name'] = !empty($data['product_name']) ? $data['product_name'] : 'Product Name Not Specified';
        $values['unit'] = !empty($data['unit']) ? $data['unit'] : '';
        $values['image_url'] = !empty($data['image_url']) ? $data['image_url'] : '';
        $values['low_stock_count'] = !empty($data['low_stock_count']) ? $data['low_stock_count'] : 0;
        $values['high_stock_count'] = !empty($data['high_stock_count']) ? $data['high_stock_count'] : 0;
        $values['product_brand_id'] = !empty($data['product_brand_id']) ? $data['product_brand_id'] : 0;
        $values['product_category_id'] = !empty($data['product_category_id']) ? $data['product_category_id'] : 0;
        $values['delete_flag'] = (!empty($data['delete_flag']) && $data['delete_flag'] === 'y') ? 'y' : 'n';

        return $values;
    }

    public function filterValuesForUpdate($data)
    {
        $values = [];

        if (!empty($data['barcode'])) {
            $values['barcode'] = $data['barcode'];
        }

        $values['product_name'] = !empty($data['product_name']) ? $data['product_name'] : 'Product Name Not Specified';

        if (!empty($data['unit'])) {
            $values['unit'] = $data['unit'];
        }

        if (!empty($data['image_url'])) {
            $values['image_url'] = $data['image_url'];
        }

        if (!empty($data['low_stock_count'])) {
            $values['low_stock_count'] = $data['low_stock_count'];
        }

        if (!empty($data['high_stock_count'])) {
            $values['high_stock_count'] = $data['high_stock_count'];
        }

        if (!empty($data['product_brand_id'])) {
            $values['product_brand_id'] = $data['product_brand_id'];
        }

        if (!empty($data['delete_flag'])) {
            $values['delete_flag'] = $data['delete_flag'];
        }

        $values['delete_flag'] = (!empty($data['delete_flag']) && $data['delete_flag'] === 'y') ? 'y' : 'n';

        return $values;
    }

    public function getDefaultInsertColumns()
    {
        return $this->defaultInsertColumns;
    }

    public function getDefaultUpdateColumns()
    {
        return $this->defaultUpdateColumns;
    }
}