<?php

namespace Product\Controller;

use Common\AbstractClasses\AppAbstractRestfulController;
use Laminas\View\Model\JsonModel;
use Product\Model\Product;
use Product\Model\ProductsTable;
use Product\Model\ProductStocks;
use Product\Model\ProductStocksTable;

class ProductStockController extends AppAbstractRestfulController
{
    protected $ProductModel;
    protected $ProductsTable;
    protected $ProductStockModel;
    protected $ProductStocksTable;

    public function __construct(
        ProductsTable $ProductsTable,
        ProductStocksTable $ProductStocksTable
    ) {
        $this->ProductModel = new Product();
        $this->ProductsTable = $ProductsTable;
        $this->ProductStockModel = new ProductStocks();
        $this->ProductStocksTable = $ProductStocksTable;
    }

    public function update($id, $data)
    {
        $productStockData = $this->ProductStockModel->filterValuesForUpdate($data);
        $affectedProductID = $this->ProductStocksTable->updateByPrimaryKey($productStockData, $id);

        $message = !empty($affectedProductID) ? 'Successfully Updated' : 'No Changes Detected';

        return new JsonModel([
            'success' => true,
            'message' => $message,
            'data' => [
                'product_id' => $id,
                'product' => $productStockData
            ]
        ]);
    }
}