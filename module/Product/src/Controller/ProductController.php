<?php

namespace Product\Controller;

use Common\AbstractClasses\AppAbstractRestfulController;
use Laminas\View\Model\JsonModel;
use Product\Model\Product;
use Product\Model\ProductsTable;
use Product\Model\ProductStocks;
use Product\Model\ProductStocksTable;

class ProductController extends AppAbstractRestfulController
{
    protected $ProductModel;
    protected $ProductsTable;
    protected $ProductStockModel;
    protected $ProductStocksTable;

    public function __construct(
        ProductsTable $ProductsTable,
        ProductStocksTable $ProductStocksTable
    ) {
        $this->ProductModel = new Product();
        $this->ProductsTable = $ProductsTable;
        $this->ProductStockModel = new ProductStocks();
        $this->ProductStocksTable = $ProductStocksTable;
    }

    public function get($id)
    {
        $searchField = $this->params()->fromQuery('search_field');

        if ($searchField == 'barcode') {
            $product = $this->ProductsTable->get([
                'barcode' => $id, 'delete_flag' => 'n']);
            $productStocks = $this->ProductStocksTable->getAllProductStocksByProductID($product['product_id']);
            $product['product_stocks'] = $productStocks;

            // Get Stocks
            // Deduct Stocks From Oldest to Newest Stocks
            // Add Individual Quantity per Stock in Array ($product['stock_level_quantity'])

            return new JsonModel([
                'success' => true,
                'data' => [
                    'product' => $product
                ]
            ]);
        } else {
            $product = $this->ProductsTable->getProductsAndProductStocksByProductID($id);

            return new JsonModel([
                'success' => true,
                'data' => [
                    'product' => $product
                ]
            ]);
        }
    }

    public function getList()
    {
        $queryParams = $this->getRequest()->getQuery();

        if (!empty($queryParams['quantity_level'])) {
            if ($queryParams['quantity_level'] === 'high') {
                $products = $this->ProductsTable->getAllProductsAndProductStocks();
                $highStockProducts = [];

                foreach($products as $product) {
                    if ($product['high_stock_count'] <= $product['quantity'])
                        $highStockProducts[] = $product;
                }

                return new JsonModel([
                    'success' => true,
                    'data' => [
                        'products' => $highStockProducts
                    ]
                ]);
            } else if ($queryParams['quantity_level'] === 'low') {
                $products = $this->ProductsTable->getAllProductsAndProductStocks();
                $lowStockProducts = [];

                foreach($products as $product) {
                    if ($product['low_stock_count'] >= $product['quantity'])
                        $lowStockProducts[] = $product;
                }

                return new JsonModel([
                    'success' => true,
                    'data' => [
                        'products' => $lowStockProducts
                    ]
                ]);
            }
        } else if(!empty($queryParams['search_field'])) {
            $searchKeyword = (!empty($queryParams['search_keyword'])) ? $queryParams['search_keyword'] : '';
            $products = $this->ProductsTable->getAllProductsAndProductStocksByColumn($queryParams['search_field'], $searchKeyword);

            return new JsonModel([
                'success' => true,
                'data' => [
                    'products' => $products
                ]
            ]);
        } else {
            $products = $this->ProductsTable->getAllProductsAndProductStocks();

            return new JsonModel([
                'success' => true,
                'data' => [
                    'products' => $products
                ]
            ]);
        }

        return new JsonModel([
            'success' => false
        ]);
    }

    public function create($data)
    {
        $productsColumn = $this->ProductModel->getDefaultInsertColumns();
        $productData = $this->ProductModel->filterValues($data);
        $productID = $this->ProductsTable->insert($productsColumn, $productData);

        $productStocksColumn = $this->ProductStockModel->getDefaultInsertColumns();
        $productStocksData = $this->ProductStockModel->filterValuesForUpdate($data);
        $productStocksData['product_id'] = $productID;
        $productStockID = $this->ProductStocksTable->insert($productStocksColumn, $productStocksData);

        return new JsonModel([
            'success' => true,
            'data' => [
                'product_id' => $productID,
                'product_stock_id' => $productStockID,
                'product' => $productData
            ]
        ]);
    }

    public function update($id, $data)
    {
        $request = $this->getRequest();
        $queryParams = $request->getQuery();
        $action = !empty($queryParams['action']) ? $queryParams['action'] : '';

        if ($action === 'UPDATE_IMAGE_URL') {
            if (!empty($data['image_url'])) {
                $productData = ['image_url' => $data['image_url']];
                $affectedProductID = $this->ProductsTable->updateByPrimaryKey($productData, $id);
                $message = !empty($affectedProductID) ? 'Successfully Updated' : 'No Changes Detected';

                return new JsonModel([
                    'success' => true,
                    'message' => $message,
                    'data' => [
                        'product_id' => $id,
                        'product' => $productData
                    ]
                ]);
            }
        } else {
            $productData = $this->ProductModel->filterValuesForUpdate($data);
            $affectedProductID = $this->ProductsTable->updateByPrimaryKey($productData, $id);
            $productStockData = $this->ProductStockModel->filterValuesForUpdate($data);
            $this->ProductStocksTable->updateByPrimaryKey($productStockData, $id);

            $message = !empty($affectedProductID) ? 'Successfully Updated' : 'No Changes Detected';

            return new JsonModel([
                'success' => true,
                'message' => $message,
                'data' => [
                    'product_id' => $id,
                    'product' => $productData
                ]
            ]);
        }

        return new JsonModel([
            'success' => false
        ]);
    }
}