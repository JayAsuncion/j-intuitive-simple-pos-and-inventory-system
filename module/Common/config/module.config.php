<?php

namespace Common;

use Common\AbstractClasses\AppAbstractTable;

return [
    'service_manager' => [
        'factories' => [

        ],
        'invokables' => [
            AppAbstractTable::class => AppAbstractTable::class
        ]
    ]
];
