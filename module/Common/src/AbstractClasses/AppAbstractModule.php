<?php

namespace Common\AbstractClasses;

use Laminas\Mvc\ModuleRouteListener;
use Laminas\Mvc\MvcEvent;

class AppAbstractModule
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        //Attach render errors
        $eventManager->attach(MvcEvent::EVENT_RENDER_ERROR, function($e)  {
            if ($e->getParam('exception')) {
                $this->exception( $e->getParam('exception') ) ; //Custom error render function.
            }
        });
        //Attach dispatch errors
        $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, function($e)  {
            if ($e->getParam('exception')) {
                $this->exception( $e->getParam('exception') ) ;//Custom error render function.
            }
        });
    }

    private function exception($e) {
        echo "<span style='font-family: courier new; padding: 2px 5px; background:red; color: white;'> " . $e->getMessage() . '</span><br/>' ;
        echo "<pre>" . $e->getTraceAsString() . '</pre>' ;
    }
}