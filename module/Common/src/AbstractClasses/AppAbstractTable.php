<?php

namespace Common\AbstractClasses;

use Laminas\Db\TableGateway\TableGateway;

class AppAbstractTable
{
    protected $TableGateway;
    protected $primary_key = 'id';

    public function __construct(TableGateway $TableGateway)
    {
        $this->TableGateway = $TableGateway;
    }

    public function getByPrimaryKey($id, $columns = ['*'])
    {
        $select = $this->TableGateway->getSql()->select();
        $select->columns($columns);
        $select->where([$this->primary_key => $id]);
        $resultSet = $this->TableGateway->selectWith($select);

        return $resultSet->getDataSource()->current();
    }

    public function get($where, $columns = ['*'], $order = [])
    {
        $select = $this->TableGateway->getSql()->select();
        $select->columns($columns);
        $select->where($where);
        $select->order($order);
        $resultSet = $this->TableGateway->selectWith($select);

        return $resultSet->getDataSource()->current();
    }

    public function getList($where = [], $columns = ['*'])
    {
        $select = $this->TableGateway->getSql()->select();
        $select->columns($columns);
        $select->where($where);
        $resultSet = $this->TableGateway->selectWith($select);

        return iterator_to_array($resultSet->getDataSource());
    }

    public function insert($columns, $values)
    {
        $insert = $this->TableGateway->getSql()->insert();
        $insert->columns($columns);
        $insert->values($values);
        $this->TableGateway->insertWith($insert);

        return $this->TableGateway->getLastInsertValue();
    }

    public function updateByPrimaryKey($set, $id)
    {
        $update = $this->TableGateway->getSql()->update();
        $update->set($set);
        $update->where([$this->primary_key => $id]);
        return $this->TableGateway->updateWith($update);
    }

    public function update($set, $where)
    {
        $update = $this->TableGateway->getSql()->update();
        $update->set($set);
        $update->where($where);
        return $this->TableGateway->updateWith($update);
    }
}