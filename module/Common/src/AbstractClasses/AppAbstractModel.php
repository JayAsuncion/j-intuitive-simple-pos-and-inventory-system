<?php

namespace Common\AbstractClasses;

class AppAbstractModel
{
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}