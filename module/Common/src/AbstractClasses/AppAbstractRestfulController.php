<?php

namespace Common\AbstractClasses;

use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractRestfulController;
use Laminas\View\Model\JsonModel;

class AppAbstractRestfulController extends AbstractRestfulController
{
    protected $collectionOptions = array();
    protected $resourceOptions = array();
    protected $eventIdentifier = 'CommonController';

    public function options()
    {
        $response = $this->getResponse();

        // if in Options array, Allow
        $response->getHeaders()->addHeaderLine('Access-Control-Allow-Methods', implode(',', $this->_getOptions()));

        return $response;
    }

    protected function _getOptions()
    {
        if ($this->params()->fromRoute($this->getIdentifierName(), false)) {
            return $this->resourceOptions;
        }

        return $this->collectionOptions;
    }

    public function get($id)
    {
        return $this->methodNotAllowed();
    }

    public function getList()
    {
        return $this->methodNotAllowed();
    }

    public function update($id, $data)
    {
        return $this->methodNotAllowed();
    }

    public function delete($id)
    {
        return $this->methodNotAllowed();
    }

    private function methodNotAllowed()
    {
        $this->getResponse()->setStatusCode(405);
        $message = 'Method Not Allowed';

        return new JsonModel([
            'success' => false,
            'message' => $message
        ]);
    }

    protected function validationError($customMessage)
    {
        $this->getResponse()->setStatusCode(412);
        $message = !empty($customMessage) ? $customMessage : 'Validation Error';

        return new JsonModel([
            'success' => false,
            'message' => $message
        ]);
    }

    protected function resourceNotFoundError($customMessage)
    {
        $this->getResponse()->setStatusCode(404);
        $message = !empty($customMessage) ? $customMessage : 'Resource not found';

        return new JsonModel([
            'success' => false,
            'message' => $message
        ]);
    }
}