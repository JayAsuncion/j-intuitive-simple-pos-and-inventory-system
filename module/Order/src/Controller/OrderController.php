<?php

namespace Order\Controller;

use Common\AbstractClasses\AppAbstractRestfulController;
use Laminas\View\Model\JsonModel;
use Order\Model\Order;
use Order\Model\OrderItem;
use Order\Model\OrderItemsTable;
use Order\Model\OrdersTable;
use Product\Model\ProductsTable;
use Product\Model\ProductStocksTable;

class OrderController extends AppAbstractRestfulController
{
    protected $OrderModel;
    protected $OrdersTable;
    protected $OrderItemModel;
    protected $OrderItemsTable;
    protected $ProductsTable;
    protected $ProductStocksTable;

    public function __construct(
        OrdersTable  $OrdersTable,
        OrderItemsTable $OrderItemsTable,
        ProductsTable $ProductsTable,
        ProductStocksTable $ProductStocksTable
    ) {
        $this->OrderModel = new Order();
        $this->OrdersTable = $OrdersTable;
        $this->OrderItemModel = new OrderItem();
        $this->OrderItemsTable = $OrderItemsTable;
        $this->ProductsTable = $ProductsTable;
        $this->ProductStocksTable = $ProductStocksTable;
    }

    public function create($data)
    {
        $cartItems = $data['cart_items'];
        $subTotal = 0;
        $discount = !empty($data['customer_info']['discount']) ? $data['customer_info']['discount'] : 0;
        $cash = !empty($data['customer_info']['cash']) ? $data['customer_info']['cash'] : 0;
        $total = 0;
        $change = 0;

        // Loop Cart Items
        // Fetch Product and Product Stocks Info, while
        // Compute Order Details (Subtotal, Discount, Total, Cash, Change) and Order Items
        foreach ($cartItems as $barcode => $cartItem) {
            $productStock = $this->ProductStocksTable->get(['product_id' => $cartItem['product_id']]);

            if (
                empty($cartItem['quantity']) ||
                empty($productStock['unit_price'])
            ) {
                $this->validationError('One of the cart items has invalid quantity or unit price');
            }

            // Validate Cart Item Data
            $cartItems[$barcode]['unit_cost'] = $productStock['unit_cost'];
            $cartItems[$barcode]['unit_price'] = $productStock['unit_price'];
            $cartItems[$barcode]['total_price'] = ($cartItem['quantity'] * $productStock['unit_price']);

            $subTotal += $cartItems[$barcode]['total_price'];
        }

        $total = $subTotal - $discount;
        $change = $cash - $total;

        // Prepare Order field values
        $order = [
            'customer_name' => !empty($data['customer_info']['customer_name']) ? $data['customer_info']['customer_name'] : 'Not specified',
            'subtotal' => $subTotal,
            'discount' => $discount,
            'total' => $total,
            'cash' => $cash,
            'change' => $change
        ];
        $ordersColumn = $this->OrderModel->getDefaultInsertColumns();
        $orderData = $this->OrderModel->filterValues($order);
        // Create Order
        $orderID = $this->OrdersTable->insert($ordersColumn, $orderData);

        // Create Order Items, While Deducting Stocks
        $orderItemsColumn = $this->OrderItemModel->getDefaultInsertColumns();
        foreach ($cartItems as $barcode => $cartItem) {
            $cartItem['order_id'] = $orderID;
            $cartItemData = $this->OrderItemModel->filterValues($cartItem);
            $productStock = $this->ProductStocksTable->get(['product_stock_id' => $cartItem['product_stock_id']]);

            // Stock quantity minus cart item quantity
            $newProductStockQuantity = $productStock['quantity'] - $cartItem['quantity'];

            $this->ProductStocksTable->updateByPrimaryKey(['quantity' => $newProductStockQuantity], $cartItem['product_stock_id']);
            $this->OrderItemsTable->insert($orderItemsColumn, $cartItemData);
        };

        return new JsonModel([
            'data' => [
                'order_id' => $orderID,
                'order' => $orderData
            ],
            'message' => 'Sales #' . $orderID . ' created successfully'
        ]);
    }

    public function getList()
    {
        // Used for Net Profit Report
        $queryParams = $this->getRequest()->getQuery();

        if (!empty($queryParams['fromDateTimestamp']) && !empty($queryParams['toDateTimestamp'])) {
            $fromDate = date(date('Y-m-d H:i:s', ($queryParams['fromDateTimestamp'] / 1000)));
            $toDate = date(date('Y-m-d H:i:s', ($queryParams['toDateTimestamp'] / 1000)));
            $orders = $this->OrdersTable->getAllOrdersBetweenDates($fromDate, $toDate);

            return new JsonModel([
                'success' => true,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'data' => [
                    'orders_length' => count($orders),
                    'orders' => $orders
                ]
            ]);
        } else {
            $orders = $this->OrdersTable->getAllOrders();

            return new JsonModel([
                'success' => true,
                'data' => [
                    'orders_length' => count($orders),
                    'orders' => $orders
                ]
            ]);
        }

        return new JsonModel([
            'success' => false
        ]);
    }

    public function update($id, $data)
    {
        if (!empty($data['delete_flag'])) {
            $orderData = [
                'delete_flag' => $data['delete_flag']
            ];
            $this->OrdersTable->updateByPrimaryKey($orderData, $id);
        }

        return new JsonModel([
            'success' => true
        ]);
    }
}