<?php

namespace Order\ServiceFactory\Controller;

use Order\Controller\OrderController;
use Order\Model\OrderItemsTable;
use Order\Model\OrdersTable;
use Product\Model\ProductsTable;
use Product\Model\ProductStocksTable;
use Psr\Container\ContainerInterface;

class OrderControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $OrdersTable = $container->get(OrdersTable::class);
        $OrderItemsTable = $container->get(OrderItemsTable::class);
        $ProductsTable = $container->get(ProductsTable::class);
        $ProductStocksTable = $container->get(ProductStocksTable::class);

        return new OrderController(
            $OrdersTable,
            $OrderItemsTable,
            $ProductsTable,
            $ProductStocksTable
        );
    }
}