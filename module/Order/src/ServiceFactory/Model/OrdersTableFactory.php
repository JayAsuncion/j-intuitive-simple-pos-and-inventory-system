<?php

namespace Order\ServiceFactory\Model;

use Laminas\Db\ResultSet\ResultSet;
use Laminas\Db\TableGateway\TableGateway;
use Order\Model\Order;
use Order\Model\OrdersTable;
use Psr\Container\ContainerInterface;

class OrdersTableFactory
{
    public function __invoke(ContainerInterface  $container)
    {
        $dbAdapter = $container->get('j-intuitive-simple-pos-and-inventory-system');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Order());

        $tableGateway = new TableGateway(
            'orders',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new OrdersTable($tableGateway);
    }
}