<?php

namespace Order\ServiceFactory\Model;

use Laminas\Db\ResultSet\ResultSet;
use Laminas\Db\TableGateway\TableGateway;
use Order\Model\OrderItem;
use Order\Model\OrderItemsTable;
use Psr\Container\ContainerInterface;

class OrderItemsTableFactory
{
    public function __invoke(ContainerInterface  $container)
    {
        $dbAdapter = $container->get('j-intuitive-simple-pos-and-inventory-system');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new OrderItem());

        $tableGateway = new TableGateway(
            'order_items',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new OrderItemsTable($tableGateway);
    }
}