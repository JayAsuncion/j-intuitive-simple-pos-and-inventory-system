<?php

namespace Order\Model;

use Common\AbstractClasses\AppAbstractTable;
use Laminas\Db\Sql\Expression;
use Laminas\Db\TableGateway\TableGateway;

class OrdersTable extends AppAbstractTable
{
    public function __construct(TableGateway $TableGateway)
    {
        parent::__construct($TableGateway);

        $this->primary_key = 'order_id';
    }

    public function getAllOrders()
    {
        $columns = [
            '*',
            'order_item_id' => new Expression('oi.order_item_id'),
            'product_id' => new Expression('oi.product_id'),
            'product_stock_id' => new Expression('oi.product_stock_id'),
            'unit_cost' => new Expression('oi.unit_cost'),
            'quantity' => new Expression('oi.quantity'),
            'unit_price' => new Expression('oi.unit_price'),
            'total_price' => new Expression('oi.total_price'),
            'product_name' => new Expression('p.product_name'),
            'unit' => new Expression('p.unit'),
        ];
        $where = [
            'orders.delete_flag' => 'n'
        ];
        $select = $this->TableGateway->getSql()->select();
        $select->columns($columns);
        $select->where($where);
        $select->order(['date_created' => 'DESC']);
        $select->join(['oi' => 'order_items'], 'orders.order_id = oi.order_id', [], 'INNER');
        $select->join(['p' => 'products'], 'oi.product_id = p.product_id', [], 'INNER');
        $resultSet = $this->TableGateway->selectWith($select);

        return iterator_to_array($resultSet->getDataSource());
    }

    public function getAllOrdersBetweenDates($fromDate, $toDate)
    {
        $columns = [
            '*',
            'order_item_id' => new Expression('oi.order_item_id'),
            'product_id' => new Expression('oi.product_id'),
            'product_stock_id' => new Expression('oi.product_stock_id'),
            'unit_cost' => new Expression('oi.unit_cost'),
            'quantity' => new Expression('oi.quantity'),
            'unit_price' => new Expression('oi.unit_price'),
            'total_price' => new Expression('oi.total_price'),
            'product_name' => new Expression('p.product_name'),
            'unit' => new Expression('p.unit'),
        ];
        $where = [
            'orders.delete_flag' => 'n'
        ];
        $select = $this->TableGateway->getSql()->select();
        $select->columns($columns);
        $select->where($where);
        $select->where->and;
        $select->where->greaterThanOrEqualTo('orders.date_created', $fromDate);
        $select->where->and;
        $select->where->lessThanOrEqualTo('orders.date_created', $toDate);
        $select->join(['oi' => 'order_items'], 'orders.order_id = oi.order_id', [], 'INNER');
        $select->join(['p' => 'products'], 'oi.product_id = p.product_id', [], 'INNER');
        $resultSet = $this->TableGateway->selectWith($select);

        return iterator_to_array($resultSet->getDataSource());
    }
}