<?php

namespace Order\Model;

use Common\AbstractClasses\AppAbstractModel;

class OrderItem extends AppAbstractModel
{
    public $order_item_id;
    public $order_id;
    public $product_id;
    public $product_stock_id;
    public $quantity;
    public $unit_cost;
    public $unit_price;
    public $total_price;

    private $defaultInsertColumns = ['order_id', 'product_id', 'product_stock_id', 'quantity', 'unit_cost', 'unit_price', 'total_price'];
    private $defaultUpdateColumns = ['order_id', 'product_id', 'product_stock_id', 'quantity', 'unit_cost', 'unit_price', 'total_price'];

    public function exchangeArray($data)
    {
        $this->order_id = !empty($data['order_id']) ? $data['order_id'] : 0;
        $this->product_id = !empty($data['product_id']) ? $data['product_id'] : 0;
        $this->product_stock_id = !empty($data['product_stock_id']) ? $data['product_stock_id'] : 0;
        $this->quantity = !empty($data['quantity']) ? $data['quantity'] : 0;
        $this->unit_cost = !empty($data['unit_cost']) ? $data['unit_cost'] : 0;
        $this->unit_price = !empty($data['unit_price']) ? $data['unit_price'] : 0;
        $this->total_price = !empty($data['total_price']) ? $data['total_price'] : 0;
    }

    public function filterValues($data)
    {
        $values = [];

        $values['order_id'] = !empty($data['order_id']) ? $data['order_id'] : 0;
        $values['product_id'] = !empty($data['product_id']) ? $data['product_id'] : 0;
        $values['product_stock_id'] = !empty($data['product_stock_id']) ? $data['product_stock_id'] : 0;
        $values['quantity'] = !empty($data['quantity']) ? $data['quantity'] : 0;
        $values['unit_cost'] = !empty($data['unit_cost']) ? $data['unit_cost'] : 0;
        $values['unit_price'] = !empty($data['unit_price']) ? $data['unit_price'] : 0;
        $values['total_price'] = !empty($data['total_price']) ? $data['total_price'] : 0;

        return $values;
    }

    public function getDefaultInsertColumns()
    {
        return $this->defaultInsertColumns;
    }

    public function getDefaultUpdateColumns()
    {
        return $this->defaultUpdateColumns;
    }
}