<?php

namespace Order\Model;

use Common\AbstractClasses\AppAbstractModel;

class Order extends AppAbstractModel
{
    public $order_id;
    public $customer_name;
    public $subtotal;
    public $discount;
    public $total;
    public $cash;
    public $change;
    public $date_created;

    private $defaultInsertColumns = ['customer_name', 'subtotal', 'discount', 'total', 'cash', 'change'];
    private $defaultUpdateColumns = ['customer_name', 'subtotal', 'discount', 'total', 'cash', 'change'];

    public function exchangeArray($data)
    {
        $this->order_id = !empty($data['order_id']) ? $data['order_id'] : 0;
        $this->customer_name = !empty($data['customer_name']) ? $data['customer_name'] : 'Not Specified';
        $this->subtotal = !empty($data['subtotal']) ? $data['subtotal'] : 0;
        $this->discount = !empty($data['discount']) ? $data['discount'] : 0;
        $this->total = !empty($data['total']) ? $data['total'] : 0;
        $this->cash = !empty($data['cash']) ? $data['cash'] : 0;
        $this->change = !empty($data['change']) ? $data['change'] : 0;
        $this->date_created = !empty($data['date_created']) ? $data['date_created'] : 0;
    }

    public function filterValues($data)
    {
        $values = [];

        $values['customer_name'] = !empty($data['customer_name']) ? $data['customer_name'] : 'Not Specified';
        $values['subtotal'] = !empty($data['subtotal']) ? $data['subtotal'] : 0;
        $values['discount'] = !empty($data['discount']) ? $data['discount'] : 0;
        $values['total'] = !empty($data['total']) ? $data['total'] : 0;
        $values['cash'] = !empty($data['cash']) ? $data['cash'] : 0;
        $values['change'] = !empty($data['change']) ? $data['change'] : 0;

        return $values;
    }

    public function getDefaultInsertColumns()
    {
        return $this->defaultInsertColumns;
    }

    public function getDefaultUpdateColumns()
    {
        return $this->defaultUpdateColumns;
    }
}