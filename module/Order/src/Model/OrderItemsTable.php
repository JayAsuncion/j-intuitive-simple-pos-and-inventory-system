<?php

namespace Order\Model;

use Common\AbstractClasses\AppAbstractTable;
use Laminas\Db\TableGateway\TableGateway;

class OrderItemsTable extends AppAbstractTable
{
    public function __construct(TableGateway $TableGateway)
    {
        parent::__construct($TableGateway);

        $this->primary_key = 'order_item_id';
    }
}