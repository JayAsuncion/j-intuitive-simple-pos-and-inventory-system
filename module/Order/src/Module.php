<?php

namespace Order;

use Common\AbstractClasses\AppAbstractModule;
use Laminas\ModuleManager\Feature\ConfigProviderInterface;

class Module extends AppAbstractModule implements ConfigProviderInterface
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
}
