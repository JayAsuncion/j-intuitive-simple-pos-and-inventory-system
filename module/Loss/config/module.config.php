<?php

/**
 * @see       https://github.com/laminas-api-tools/api-tools-skeleton for the canonical source repository
 * @copyright https://github.com/laminas-api-tools/api-tools-skeleton/blob/master/COPYRIGHT.md
 * @license   https://github.com/laminas-api-tools/api-tools-skeleton/blob/master/LICENSE.md New BSD License
 */

namespace Loss;

use Laminas\Router\Http\Segment;
use Loss\Controller\LossItemController;
use Loss\Model\LossItemsTable;
use Loss\ServiceFactory\Controller\LossItemControllerFactory;
use Loss\ServiceFactory\Model\LossItemsTableFactory;

return [
    'router' => [
        'routes' => [
            'loss-items' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/loss-items[/:id]',
                    'defaults' => [
                        'controller' => LossItemController::class
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            LossItemController::class => LossItemControllerFactory::class
        ],
    ],
    'service_manager' => [
        'factories' => [
            LossItemsTable::class => LossItemsTableFactory::class
        ],
        'invokables' => [

        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
