<?php

namespace Loss\Model;

use Common\AbstractClasses\AppAbstractModel;

class LossItem extends AppAbstractModel
{
    public $loss_item_id;
    public $product_id;
    public $product_stock_id;
    public $quantity;
    public $unit_cost;
    public $unit_price;
    public $total_price;
    public $type;

    private $defaultInsertColumns = ['product_id', 'product_stock_id', 'quantity', 'unit_cost', 'unit_price', 'total_price', 'type'];
    private $defaultUpdateColumns = ['product_id', 'product_stock_id', 'quantity', 'unit_cost', 'unit_price', 'total_price', 'type'];

    public function exchangeArray($data)
    {
        $this->product_id = !empty($data['product_id']) ? $data['product_id'] : 0;
        $this->product_stock_id = !empty($data['product_stock_id']) ? $data['product_stock_id'] : 0;
        $this->quantity = !empty($data['quantity']) ? $data['quantity'] : 0;
        $this->unit_cost = !empty($data['unit_cost']) ? $data['unit_cost'] : 0;
        $this->unit_price = !empty($data['unit_price']) ? $data['unit_price'] : 0;
        $this->total_price = !empty($data['total_price']) ? $data['total_price'] : 0;
        $this->type = !empty($data['type']) ? $data['type'] : 'Internal Use';
    }

    public function filterValues($data)
    {
        $values = [];

        $values['product_id'] = !empty($data['product_id']) ? $data['product_id'] : 0;
        $values['product_stock_id'] = !empty($data['product_stock_id']) ? $data['product_stock_id'] : 0;
        $values['quantity'] = !empty($data['quantity']) ? $data['quantity'] : 0;
        $values['unit_cost'] = !empty($data['unit_cost']) ? $data['unit_cost'] : 0;
        $values['unit_price'] = !empty($data['unit_price']) ? $data['unit_price'] : 0;
        $values['total_price'] = !empty($data['total_price']) ? $data['total_price'] : 0;
        $values['type'] = !empty($data['type']) ? $data['type'] : 'Internal Use';

        return $values;
    }

    public function getDefaultInsertColumns()
    {
        return $this->defaultInsertColumns;
    }

    public function getDefaultUpdateColumns()
    {
        return $this->defaultUpdateColumns;
    }
}