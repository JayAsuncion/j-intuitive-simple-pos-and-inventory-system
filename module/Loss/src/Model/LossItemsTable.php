<?php

namespace Loss\Model;

use Common\AbstractClasses\AppAbstractTable;
use Laminas\Db\TableGateway\TableGateway;

class LossItemsTable extends AppAbstractTable
{
    public function __construct(TableGateway $TableGateway)
    {
        parent::__construct($TableGateway);

        $this->primary_key = 'loss_item_id';
    }
}