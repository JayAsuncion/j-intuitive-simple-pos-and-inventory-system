<?php

namespace Loss\Controller;

use Common\AbstractClasses\AppAbstractRestfulController;
use Laminas\View\Model\JsonModel;
use Loss\Model\LossItem;
use Loss\Model\LossItemsTable;

class LossItemController extends AppAbstractRestfulController
{
    protected $LossItemModel;
    protected $LossItemsTable;

    public function __construct(
        LossItemsTable $LossItemsTable
    )
    {
        $this->LossItemModel = new LossItem();
        $this->LossItemsTable = $LossItemsTable;
    }

    public function create($data)
    {
        $insertColumns = $this->LossItemModel->getDefaultInsertColumns();
        $lossItemData = $this->LossItemModel->filterValues($data);
        $lossItemID = $this->LossItemsTable->insert($insertColumns, $lossItemData);

        return new JsonModel([
            'success' => true,
            'data' => [
                'loss_item_id' => $lossItemID
            ]
        ]);
    }
}