<?php

namespace Loss\ServiceFactory\Model;

use Laminas\Db\ResultSet\ResultSet;
use Laminas\Db\TableGateway\TableGateway;
use Loss\Model\LossItem;
use Loss\Model\LossItemsTable;
use Psr\Container\ContainerInterface;

class LossItemsTableFactory
{
    public function __invoke(ContainerInterface  $container)
    {
        $dbAdapter = $container->get('j-intuitive-simple-pos-and-inventory-system');
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new LossItem());

        $tableGateway = new TableGateway(
            'loss_items',
            $dbAdapter,
            null,
            $resultSetPrototype
        );

        return new LossItemsTable($tableGateway);
    }
}