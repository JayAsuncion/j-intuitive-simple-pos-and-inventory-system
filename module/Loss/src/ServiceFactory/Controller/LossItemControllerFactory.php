<?php

namespace Loss\ServiceFactory\Controller;

use Loss\Controller\LossItemController;
use Loss\Model\LossItemsTable;
use Psr\Container\ContainerInterface;

class LossItemControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $LossItemsTable = $container->get(LossItemsTable::class);

        return new LossItemController(
            $LossItemsTable
        );
    }
}