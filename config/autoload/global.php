<?php

/**
 * @see       https://github.com/laminas-api-tools/api-tools-skeleton for the canonical source repository
 * @copyright https://github.com/laminas-api-tools/api-tools-skeleton/blob/master/COPYRIGHT.md
 * @license   https://github.com/laminas-api-tools/api-tools-skeleton/blob/master/LICENSE.md New BSD License
 */

$globalConfig = [
    'api-tools-content-negotiation' => [
        'selectors' => [],
    ],
    'db' => [
        'adapters' => [
            'j-intuitive-simple-pos-and-inventory-system' => [
                'driver' => 'Pdo',
                'dsn' => 'mysql:dbname=j-intuitive-simple-pos-and-inventory-system;host=localhost;port=3306',
                'driver_options' => [
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
                ],
                'username' => 'root',
                'password' => '',
            ],
        ],
    ],
];

if (getenv('ENVIRONMENT') === "Production") {
    $prodDSN = 'mysql:dbname=u965685285_simple_pos;host=localhost;port=3306';
    $prodUserName = 'u965685285_simple_pos';
    $prodPassword = 'L$2P6eva2Vs';
    $globalConfig['db']['adapters']['j-intuitive-simple-pos-and-inventory-system']['dsn'] = $prodDSN;
    $globalConfig['db']['adapters']['j-intuitive-simple-pos-and-inventory-system']['username'] = $prodUserName;
    $globalConfig['db']['adapters']['j-intuitive-simple-pos-and-inventory-system']['password'] = $prodPassword;
}

return $globalConfig;
