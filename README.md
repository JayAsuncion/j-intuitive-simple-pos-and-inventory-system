J Intuitive Simple POS Angular App
======================================

Author: [Christian Jay D. Asuncion](https://gitlab.com/JayAsuncion)

Prerequisite
------------
- PHP Version >= 7.3.0
- Domain: 1
- Subdomain: 2
    - API
    - Image Bucket
    

Installation
------------
### Composer
```bash
$ composer install
```

### Config
Go to **/config/autoload/global.php**.

Update the following to your own Database Config. (Example below)
- **$prodDSN**, 
- **$prodUserName**,
- **$prodPassword**


```php
$globalConfig = [
    'api-tools-content-negotiation' => [
        'selectors' => [],
    ],
    'db' => [
        'adapters' => [
            'j-intuitive-simple-pos-and-inventory-system' => [
                'driver' => 'Pdo',
                'dsn' => 'mysql:dbname=j-intuitive-simple-pos-and-inventory-system;host=localhost;port=3306',
                'driver_options' => [
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
                ],
                'username' => 'root',
                'password' => '',
            ],
        ],
    ],
];

if (getenv('ENVIRONMENT') === "Production") {
    $prodDSN = 'mysql:dbname=u965685285_simple_pos;host=localhost;port=3306';
    $prodUserName = 'u965685285_simple_pos';
    $prodPassword = 'L$2P6eva2Vs';
    $globalConfig['db']['adapters']['j-intuitive-simple-pos-and-inventory-system']['dsn'] = $prodDSN;
    $globalConfig['db']['adapters']['j-intuitive-simple-pos-and-inventory-system']['username'] = $prodUserName;
    $globalConfig['db']['adapters']['j-intuitive-simple-pos-and-inventory-system']['password'] = $prodPassword;
}

return $globalConfig;
```

### .htaccess
There is a **.htaccess** file on the **root** directory
(same level with composer.json).
This is used to **rewrite request** and point it inside the **/public**
directory.

Append this rewrite codes to force HTTPS if necessary.
[More info on force HTTPS](https://www.hostinger.ph/tutorials/ssl/forcing-https)

```apacheconfig
RewriteEngine on
RewriteCond %{HTTPS} off
RewriteCond %{HTTP_HOST} (www\.)?api.officialnusg.com
RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]
```

The .htaccess inside **/public** directory is the **default of Laminas**.

Appended with SetEnv ENVIRONMENT "Production".
```apacheconfig
SetEnv ENVIRONMENT "Production";
```

### Database 

The **SQL dump** file can be found in the **root** directory with name
**simple_pos_empty.sql**.

- It has stub **products** and **product_stocks** inside.
- Empty **orders**, **order_items** and **loss_items**.
- Unused tables include **unit_lockup**

------------

### Do at your own risk
Comment these codes if you want to bypass PHP version check.

It can be found inside **/vendor/composer/platform_check.php**.

```php
if ($missingExtensions) {
    $issues[] = 'Your Composer dependencies require the following PHP extensions to be installed: ' . implode(', ', $missingExtensions);
}
```
