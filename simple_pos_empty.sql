/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 10.4.14-MariaDB : Database - j-intuitive-simple-pos-and-inventory-system
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `loss_items` */

DROP TABLE IF EXISTS `loss_items`;

CREATE TABLE `loss_items` (
  `loss_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `product_stock_id` int(11) NOT NULL,
  `unit_cost` decimal(10,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` decimal(10,2) NOT NULL,
  `total_price` decimal(10,2) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT current_timestamp(),
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`loss_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4;

/*Data for the table `loss_items` */

/*Table structure for table `order_items` */

DROP TABLE IF EXISTS `order_items`;

CREATE TABLE `order_items` (
  `order_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_stock_id` int(11) NOT NULL,
  `unit_cost` decimal(10,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` decimal(10,2) NOT NULL,
  `total_price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`order_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;

/*Data for the table `order_items` */

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(255) DEFAULT NULL,
  `subtotal` decimal(10,2) DEFAULT NULL,
  `discount` decimal(10,2) DEFAULT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `cash` decimal(10,2) DEFAULT NULL,
  `change` decimal(10,2) DEFAULT NULL,
  `date_created` datetime DEFAULT current_timestamp(),
  `delete_flag` char(1) DEFAULT 'n',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4;

/*Data for the table `orders` */

/*Table structure for table `product_stocks` */

DROP TABLE IF EXISTS `product_stocks`;

CREATE TABLE `product_stocks` (
  `product_stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `unit_cost` decimal(10,2) DEFAULT NULL,
  `unit_price` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`product_stock_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `product_stocks` */

insert  into `product_stocks`(`product_stock_id`,`product_id`,`quantity`,`unit_cost`,`unit_price`) values 
(4,10,0.00,0.50,1.00),
(5,11,0.00,6.00,8.00),
(6,12,0.00,12.00,15.00),
(7,13,1.00,12.00,15.00);

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `barcode` varchar(50) NOT NULL,
  `product_name` longtext NOT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `high_stock_count` decimal(10,2) DEFAULT NULL,
  `low_stock_count` decimal(10,2) DEFAULT NULL,
  `product_brand_id` int(11) DEFAULT 0,
  `product_category_id` int(11) DEFAULT 0,
  `delete_flag` char(1) DEFAULT 'n',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

/*Data for the table `products` */

insert  into `products`(`product_id`,`barcode`,`product_name`,`unit`,`image_url`,`high_stock_count`,`low_stock_count`,`product_brand_id`,`product_category_id`,`delete_flag`) values 
(10,'4800365101017','Super Crunch','pc','http://local-images.j-intuitive-simple-pos-and-inventory-system.com//product-images/Super Crunch_1608435099.png',500.00,50.00,0,0,'n'),
(11,'4800016642029','Chippy Barbeque','27g','http://local-images.j-intuitive-simple-pos-and-inventory-system.com//product-images/Chippy_1608435277.jpg',200.00,50.00,0,0,'n'),
(12,'4800016643095','Chippy Barbeque','110g','http://local-images.j-intuitive-simple-pos-and-inventory-system.com//product-images/Chippy Large_1608435402.jpg',200.00,50.00,0,0,'n'),
(13,'4800016671500','Piatos Sour Cream & Onion','85g','http://local-images.j-intuitive-simple-pos-and-inventory-system.com//product-images/Piatos Green_1608435592.jpg',200.00,50.00,0,0,'n');

/*Table structure for table `unit_lookup` */

DROP TABLE IF EXISTS `unit_lookup`;

CREATE TABLE `unit_lookup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `unit_lookup` */

insert  into `unit_lookup`(`id`,`unit`) values 
(1,'PC');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
